#!/bin/sh

# governor - set the CPU governor (shortcut
#
# Available governors:
#
#   - performance   use maximum frequency.
#   - powersave     use minimum frequency. This is the default for Intel
#                   CPUs using the intel_pstate driver (Sandy Bridge and
#                   newer).
#   - userspace     use user specified frequencies.
#   - ondemand      scale the frequency dynamically according to current
#                   load. Jump to the highest frequency and then
#                   possibly back off as the idle time increases. This
#                   is the default for AMD and older Intel CPUs.
#   - conservative  scale the frequency dynamically according to current
#                   load. Scale the frequency more gradually than
#                   ondemand.
#   - schedutil     use scheduler-driven CPU frequency selection.
#
# Based on ArchWiki:
#   <wiki.archlinux.org/index.php/CPU_frequency_scaling>
#

subcommand="${1}"
governor="${2}"

get_governor() {
  local governor="$(
    cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
  )"

  echo "${governor}"
}

set_governor() {
  local governor="${1}"

  case "${governor}" in
    "performance"  | \
    "powersave"    | \
    "userspace"    | \
    "ondemand"     | \
    "conservative" | \
    "schedutil"    )
      cpupower frequency-set --governor "${governor}";;
    *) msg_error "set ${governor}: there is no such governor.";;
  esac
}

msg_error() {
  local message="${1:-asd}"
  echo "${message}"
  exit 1
}

case "${subcommand}" in
  "get") get_governor;;
  "set") set_governor "${governor}";;
      *) msg_error "Usage: get|set GOVERNOR";;
esac
