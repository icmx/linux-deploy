#!/usr/bin/python3

'''
addrecent - dumb script to add file(s) into GTK-compatible global list
of recently used elements (e.g. "Recent" tab in Nautilus).

Example

  addrecent file-one.png file-two.txt

  This will add two files into the recently used elements list.

How It Works

  From what I know, recently used elements (files) are listed in XML
  file located in ~/.local/share/recently-used.xbel. It has relatively
  simple structure:

    <xbel>
      <bookmark href="" added="" modified="" visited="">
        <info>
          <metadata owner="">
            <mime:mime-type type="" />
            <bookmark:applications>
              <bookmark:application
               name="" exec="" modified="" count="" />
            </bookmark:applications>
          </metadata>
        </info>
      </bookmark>
      <bookmark> ... </bookmark>
      <bookmark> ... </bookmark>
    </xbel>

Notes:

  - <bookmark>'s href is full path to actual recently used file, e.g.
    /home/curly/Pictures/dragon.jpg
  - <metadata>'s owner is constant URL value (http://freedesktop.org)
  - <mime:mime-type>'s type is RFC MIME type of recently used file
  - <bookmark:applications> includes zero or more
    <bookmark:application> elements
  - Each <bookmark:application> has name, exec and count attributes:
  - name is full name of application which will open that recently used
    file (e.g, GNU Image Manipulation Program). Looks like it's not
    mandatory value.
  - exec is the command to open file by application noted above. It must
    be surrounded by ' characters and use %u for target file name. For
    instance, 'gimp-2.10%u' is valid. I personally use xdg-open due to
    its universality (and it keeps script logic simple)
  - count is number of access times to file (seems to be, at least)
  - Some elements have added, modified and visited attributes which are
    always timestamps formatted as ISO 8601 string (
    YYYY-mm-ddTHH:MM:SSZ), like added="1970-01-01T13:37:00Z". These
    values may be equal.
  - There are another attributes and elements, but they're not
    neccessary.

That is, to add a new file into recently used list, one have to create
new basic <bookmark> element with all the contents noted above and add
in into XML document root, which is <xbel>. If such file already exists,
one should uncrease count attribute by 1.

'''

from xml.etree    import ElementTree
from mimetypes    import MimeTypes
from datetime     import datetime
from sys          import argv
from sys          import stderr
from os.path      import abspath
from sys          import stdout
from os           import environ
from os.path      import isfile
from urllib.parse import quote

USER          = environ.get('HOME')
XDG_DATA_HOME = environ.get(
    'XDG_DATA_HOME', '{0}/.local/share'.format(USER)
)
RECENTLY_USED = '{0}/recently-used.xbel'.format(XDG_DATA_HOME)

NS_MIME     = 'http://www.freedesktop.org/standards/shared-mime-info'
NS_BOOKMARK = 'http://www.freedesktop.org/standards/desktop-bookmarks'

def toout(text):
    time = get_datetime()
    print('addrecent: {0}: {1}'.format(time, text), file=stdout)

def toerr(text):
    time = get_datetime()
    print('addrecent: {0}: {1}'.format(time, text), file=stderr)

def get_datetime():
    return datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')

def get_mimetype(path):
    return MimeTypes().guess_type(path)[0] or 'application/octet-stream'

def get_fullpath(path):
    return 'file://{path}'.format(path=quote(abspath(path)))

def main(args):
    ElementTree.register_namespace('mime',     NS_MIME)
    ElementTree.register_namespace('bookmark', NS_BOOKMARK)

    tree = ElementTree.parse(RECENTLY_USED)
    root = tree.getroot()

    for o in args:
        if isfile(o):

            toout('{target}: adding...'.format(target=o))

            fullpath  = get_fullpath(o)
            timestamp = get_datetime()
            mimetype  = get_mimetype(fullpath)

            element = root.find('./bookmark[@href=\'{0}\']'.format(
                fullpath)
            )

            if element:
                bookmark_application = element.find(
                    './info/metadata/{{{0}}}applications/{{{0}}}application'.format(NS_BOOKMARK)
                )
                count = int(bookmark_application.get('count'))
                bookmark_application.set('count', str(count + 1))
            else:
                bookmark = ElementTree.SubElement(root, 'bookmark', {
                    'href':     fullpath,
                    'added':    timestamp,
                    'modified': timestamp,
                    'visited':  timestamp,
                })

                info = ElementTree.SubElement(bookmark, 'info')
                metadata = ElementTree.SubElement(info, 'metadata', {
                    'owner': 'http://freedesktop.org'
                })

                mime_mimetype = ElementTree.SubElement(
                    metadata,
                    '{{{0}}}mime-type'.format(NS_MIME),
                    { 'type': mimetype }
                )

                bookmark_applications = ElementTree.SubElement(
                    metadata,
                    '{{{0}}}applications'.format(NS_BOOKMARK)
                )

                bookmark_application = ElementTree.SubElement(
                    bookmark_applications,
                    '{{{0}}}application'.format(NS_BOOKMARK),
                    {
                        'name': 'xdg-open',
                        'exec': '\'xdg-open %u\'',
                        'count': '1',
                        'modified': timestamp,
                    }
                )
        else:
            toerr('{target}: not a file!'.format(target=o))

        tree.write(
             RECENTLY_USED,
             xml_declaration=True,
             encoding='utf-8',
             method="xml"
        )

if __name__ == '__main__':
    if len(argv) == 1:
        toerr('Too few arguments (add some file names to command).')
    else:
        main(argv[1:])
