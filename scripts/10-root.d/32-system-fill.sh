set -e
echo "<root> Filling system configuration ..."

echo "export ODG_PLATFORM=\"${O_PLATFORM}\"" >> "/etc/profile.d/16-custom-variables.sh"
echo -e "127.0.0.1\tic.o" >> "/etc/hosts"

groupadd "odg"
usermod --append --groups "odg" "${O_NONROOT}"


# Generating CP 1251 locale. This is a cyrillic locale for some of old
# Windows software in Russian, I use it through Wine.

echo "ru_RU.CP1251" >> "/etc/default/libc-locales"
xbps-reconfigure --force glibc-locales

# Now it should be launched like
#
#   LANG="ru_RU.CP1251" wine software-name


# Disabling NVidia graphic card for laptop
#
# One can enable it back by executing the following command as root:
#
#   tee /proc/acpi/bbswitch <<<ON
#

if [[ "${O_PLATFORM}" = "laptop" ]]; then
  mkdir --parents "/etc/modules-load.d"

  echo "bbswitch load_state=0" >> "/etc/modprobe.d/bbswitch.conf"
  echo "bbswitch"              >> "/etc/modules-load.d/bbswitch.conf"
fi
